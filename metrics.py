
import torch
import numpy as np
import os

class DataClass:

    def __init__(self):
        self.data_range = [0,1]
        self.yuv_data = {}
        self.rgb_data = None
        self.shape = []          # height, width

    def load_image(self, filename, def_bits = 10, def_fmt="444", device="cpu", color_conv="709"):
        ext = filename[-4:].lower()
        if ext == ".yuv":
            w,h,b,fmt = DataClass.extract_info(filename, def_bits, def_fmt)
            self.shape = [h,w]
            yuv_data = DataClass.read_yuv(filename, w, h, b, fmt=fmt, device=device, out_plane_norm=self.data_range) 

            for plane in yuv_data:
                self.yuv_data[plane] = DataClass.round_plane(yuv_data[plane], def_bits)
            
            yuv_t = self.convert_yuvdict_to_tensor(self.yuv_data)
            self.rgb_data = DataClass.yuv_to_rgb(yuv_t, color_conv)
            # Should we round RGB to n bits?
            self.rgb_data = DataClass.round_plane(self.rgb_data, def_bits)
        elif ext == ".png" or ext == ".jpg":
            from PIL import Image
            with Image.open(filename) as im:
                rgb_data = np.array(im.convert("RGB"))
            self.rgb_data = torch.tensor(rgb_data, dtype=torch.float, device=device).permute(2,0,1)
            self.rgb_data = DataClass.convert_and_round_plane(self.rgb_data, [0, 255], def_bits).unsqueeze(0)
            yuv_t = self.rgb_to_yuv(self.rgb_data, color_conv).clamp(min(self.data_range), max(self.data_range))
            yuv_t = DataClass.round_plane(yuv_t, def_bits)
            self.shape = yuv_t.shape[-2:]
            self.yuv_data = {
                'Y': yuv_t[0,0],
                'U': yuv_t[0,1],
                'V': yuv_t[0,2]
            }
        else:
            raise NotImplementedError
        return self


    @staticmethod
    def round_plane(plane, bits):
        return plane.mul((1 << bits)-1).round().div((1 << bits)-1)

    # TODO: move this function fo DataClass
    @staticmethod
    def convertup_and_round_plane(plane, cur_range, bits):
        return DataClass.convert_range(plane, cur_range, [0,1]).mul((1 << bits)-1).round()

    @staticmethod
    def convert_and_round_plane(plane, cur_range, bits):
        return DataClass.round_plane(DataClass.convert_range(plane, cur_range, [0,1]),bits) 

    @staticmethod
    def convert_range(plane, cur_range, new_range=[0,1]):
        if cur_range[0] == new_range[0] and cur_range[1] == new_range[1]:
            return plane
        return (plane + cur_range[0]) * (new_range[1] - new_range[0]) / (cur_range[1] - cur_range[0]) - new_range[0]


    @staticmethod
    def convert_yuvdict_to_tensor(yuv, device="cpu"):
        size = yuv['Y'].shape
        c = len(yuv)
        ans = torch.zeros( (1, c, size[-2], size[-1]), dtype=torch.float, device=torch.device(device))
        ans[:,0,:,:] = yuv['Y']
        ans[:,1,:,:] = yuv['U'] if 'U' in yuv else yuv['Y']
        ans[:,2,:,:] = yuv['V'] if 'V' in yuv else yuv['Y']
        return ans

    @staticmethod
    def color_conv_matrix(color_conv = "709"):
        if color_conv == "601":
            # BT.601
            a = 0.299
            b = 0.587
            c = 0.114
            d = 1.772
            e = 1.402
        elif color_conv == "709":
            # BT.709
            a = 0.2126
            b = 0.7152
            c = 0.0722
            d = 1.8556
            e = 1.5748
        elif color_conv == "2020":
            # BT.2020
            a = 0.2627
            b = 0.6780
            c = 0.0593
            d = 1.8814
            e = 1.4747
        else:
            raise NotImplementedError

        return a,b,c,d,e

    @staticmethod
    def yuv_to_rgb(image: torch.Tensor, color_conv="709") -> torch.Tensor:
        r"""Convert an YUV image to RGB.

        The image data is assumed to be in the range of (0, 1).

        Args:
            image (torch.Tensor): YUV Image to be converted to RGB with shape :math:`(*, 3, H, W)`.

        Returns:
            torch.Tensor: RGB version of the image with shape :math:`(*, 3, H, W)`.

        Example:
            >>> input = torch.rand(2, 3, 4, 5)
            >>> output = yuv_to_rgb(input)  # 2x3x4x5

        Took from https://kornia.readthedocs.io/en/latest/_modules/kornia/color/yuv.html#rgb_to_yuv
        """
        if not isinstance(image, torch.Tensor):
            raise TypeError("Input type is not a torch.Tensor. Got {}".format(
                type(image)))

        if len(image.shape) < 3 or image.shape[-3] != 3:
            raise ValueError("Input size must have a shape of (*, 3, H, W). Got {}"
                            .format(image.shape))

        y: torch.Tensor = image[..., 0, :, :]
        u: torch.Tensor = image[..., 1, :, :] - 0.5
        v: torch.Tensor = image[..., 2, :, :] - 0.5

        #r: torch.Tensor = y + 1.14 * v  # coefficient for g is 0
        #g: torch.Tensor = y + -0.396 * u - 0.581 * v
        #b: torch.Tensor = y + 2.029 * u  # coefficient for b is 0

        a,b,c,d,e = DataClass.color_conv_matrix(color_conv)
    
        
        r: torch.Tensor = y + e * v  # coefficient for g is 0
        g: torch.Tensor = y - (c * d / b) * u - (a * e / b) * v
        b: torch.Tensor = y + d * u  # coefficient for b is 0

        out: torch.Tensor = torch.stack([r, g, b], -3)

        return out


    @staticmethod
    def rgb_to_yuv(image: torch.Tensor, color_conv="709") -> torch.Tensor:
        r"""Convert an RGB image to YUV.

        The image data is assumed to be in the range of (0, 1).

        Args:
            image (torch.Tensor): RGB Image to be converted to YUV with shape :math:`(*, 3, H, W)`.

        Returns:
            torch.Tensor: YUV version of the image with shape :math:`(*, 3, H, W)`.

        Example:
            >>> input = torch.rand(2, 3, 4, 5)
            >>> output = rgb_to_yuv(input)  # 2x3x4x5
        """
        if not isinstance(image, torch.Tensor):
            raise TypeError("Input type is not a torch.Tensor. Got {}".format(type(image)))

        if len(image.shape) < 3 or image.shape[-3] != 3:
            raise ValueError("Input size must have a shape of (*, 3, H, W). Got {}".format(image.shape))

        r: torch.Tensor = image[..., 0, :, :]
        g: torch.Tensor = image[..., 1, :, :]
        b: torch.Tensor = image[..., 2, :, :]

        a1,b1,c1,d1,e1 = DataClass.color_conv_matrix(color_conv)

        #y: torch.Tensor = 0.299 * r + 0.587 * g + 0.114 * b
        #u: torch.Tensor = -0.147 * r - 0.289 * g + 0.436 * b
        #v: torch.Tensor = 0.615 * r - 0.515 * g - 0.100 * b
        y: torch.Tensor = a1 * r + b1 * g + c1 * b
        u: torch.Tensor = (b - y) / d1 + 0.5
        v: torch.Tensor = (r - y) / e1 + 0.5

        out: torch.Tensor = torch.stack([y, u, v], -3)

        return out        

    @staticmethod
    def extract_info(fn, default_bits = 10, default_fmt = '444'):
        import re
        wh = re.search("(?P<w>\d+)x(?P<h>\d+)", fn)
        b = re.search("(?P<b>\d+)bit", fn)

        w = wh.group('w')
        h = wh.group('h')

        b = default_bits if b is None else b.group('b')        
        fmt = default_fmt
        if "YUV444" in fn:
            fmt = "444"
        elif "YUV420" in fn:
            fmt = "420"
            raise NotImplementedError
            # TODO: add upsampling in YUV -> RGB convertion
        elif "sRGB" in fn:
            fmt = "sRGB"
        
        return int(w),int(h),int(b),fmt

    @staticmethod
    def read_yuv(filename, width, height, bits=8, out_plane_norm = [ 0, 1 ], fmt="444", device="cpu" ): 
        nr_bytes = int(np.ceil(bits / 8))
        if nr_bytes == 1:
            data_type = np.uint8
        elif nr_bytes == 2:
            data_type = np.uint16
        else:
            raise NotImplementedError('Reading more than 16-bits is currently not supported!')

        ans = {'Y': None, 'U': None, 'V': None}
        sizes = {'Y': [height, width], 'U': [height, width], 'V': [height, width]}

        if fmt == '420':
            for a in ['U', 'V']:
                sizes[a][0] >>= 1
                sizes[a][1] >>= 1
        elif fmt == '400':
            ans = {'Y': None}
            for a in ['U', 'V']:
                sizes[a][0] = 0
                sizes[a][1] = 0
        elif fmt == '444':
            pass
        else:
            raise NotImplementedError('The specified yuv format is not supported!')    

        for plane in ans:
            ans[plane] = torch.zeros(sizes[plane], dtype=torch.float, device=torch.device(device))
        
        with open(filename, "rb") as f:
            for plane in ['Y', 'U', 'V']:
                tmp = np.frombuffer( f.read(np.int(sizes[plane][0] * sizes[plane][1] * nr_bytes)), dtype=data_type)
                tmp = tmp.reshape(sizes[plane])

                ans[plane] = torch.tensor(( tmp.astype(np.float32) / (2**bits-1) ) * (max(out_plane_norm) - min(out_plane_norm)) + min(out_plane_norm), dtype=torch.float, device=torch.device(device))

        
        return ans


    def write_yuv(self, f, bits=8): #TODO: Tim (-1,1) everywhere
        #TODO: Tim test 10 bit
        """
        dump a yuv file to the provided path
        @path: path to dump yuv to (file must exist)
        @bits: bitdepth
        @frame_idx: at which idx to write the frame (replace), -1 to append
        """
        yuv = self.yuv_data.copy()
        nr_bytes = np.ceil(bits / 8)
        if nr_bytes == 1:
            data_type = np.uint8
        elif nr_bytes == 2:
            data_type = np.uint16
        elif nr_bytes <= 4:
            data_type = np.uint32
        else:
            raise NotImplementedError('Writing more than 16-bits is currently not supported!')


        # rescale to range of bits
        for plane in yuv:
            #yuv[plane] = np.round(yuv[plane].astype(np.float32) / (2**8-1) * (2**bits -1))
            yuv[plane] = DataClass.convertup_and_round_plane(yuv[plane], self.data_range, bits).cpu().numpy()

        # dump to file
        # Changed this part of code to support 4:0:0
        lst = []
        for plane in ['Y', 'U', 'V']:
            if plane in yuv.keys():
                lst = lst + yuv[plane].ravel().tolist()

        raw = np.array(lst)
        #raw = np.array(yuv['Y'].ravel().tolist() + yuv['U'].ravel().tolist() + yuv['V'].ravel().tolist())

        raw.astype(data_type).tofile(f)



class MetricParent:

    def __init__(self, bits=10, max_val=1023):
        self.bits = bits
        self.max_val = max_val



    def calc(self, orig, rec):
        raise NotImplementedError

class PSNRMetric(MetricParent):

    def calc(self, orig, rec):
        ans = []
        for plane in orig.yuv_data:
            a = orig.yuv_data[plane].mul((1 << self.bits)-1)
            b = rec.yuv_data[plane].mul((1 << self.bits)-1)
            mse = torch.mean((a - b) ** 2).item()
            ans.append(20 * np.log10(self.max_val) - 10 * np.log10(mse))
        return ans

class MSSSIMTorch(MetricParent):

    def calc(self, orig, rec):
        ans = 0.0
        from pytorch_msssim import ms_ssim
        if "Y" not in orig.yuv_data or "Y" not in rec.yuv_data:
            return -100.0
        plane = "Y"
        a = orig.yuv_data[plane].mul((1 << self.bits)-1)
        b = rec.yuv_data[plane].mul((1 << self.bits)-1)
        a.unsqueeze_(0).unsqueeze_(0)
        b.unsqueeze_(0).unsqueeze_(0)
        ans = ms_ssim(a, b, data_range=self.max_val).item()
        
        return ans

class MSSSIM_IQA(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards)
        from IQA_pytorch.MS_SSIM import MS_SSIM
        self.ms_ssim = MS_SSIM(channels=1)

    def calc(self, orig, rec):
        ans = 0.0
        if "Y" not in orig.yuv_data or "Y" not in rec.yuv_data:
            return -100.0
        plane = "Y"
        a = orig.yuv_data[plane].unsqueeze(0).unsqueeze(0)
        b = rec.yuv_data[plane].unsqueeze(0).unsqueeze(0)
        ans = self.ms_ssim(a, b, as_loss=False).item()
        
        return ans        

class PSNR_HVS(MetricParent):


    def calc(self, orig, rec):
        from psnr_hvs_m import psnrhvsm
        from PIL import Image

        a = orig.yuv_data['Y'].squeeze() #orig.rgb_data.squeeze().permute(1, 2, 0)
        b = rec.yuv_data['Y'].squeeze() #rec.rgb_data.squeeze().permute(1, 2, 0)
        a = DataClass.convert_range(a, orig.data_range, [0,255])
        b = DataClass.convert_range(b, orig.data_range, [0,255])
        #a_img = Image.fromarray(a.cpu().numpy(), 'RGB')
        #b_img = Image.fromarray(b.cpu().numpy(), 'RGB')
        a_img = a.cpu().numpy()
        b_img = b.cpu().numpy()

        p_hvs_m, p_hvs = psnrhvsm(a_img, b_img)
        
        return p_hvs_m      

class VIF_IQA(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards)
        from IQA_pytorch import VIFs
        self.vif = VIFs(channels=1)

    def calc(self, orig, rec):
        ans = 0.0
        if "Y" not in orig.yuv_data or "Y" not in rec.yuv_data:
            return -100.0
        plane = "Y"
        a = orig.yuv_data[plane].unsqueeze(0).unsqueeze(0)
        b = rec.yuv_data[plane].unsqueeze(0).unsqueeze(0)
        self.vif = self.vif.to(a.device)
        ans = self.vif(a, b, as_loss=False).item()
        
        return ans        

class FSIM_IQA(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards)
        from IQA_pytorch import FSIM
        self.fsim = FSIM(channels=3)

    def calc(self, orig, rec):
        ans = 0.0
        
        a = orig.rgb_data
        b = rec.rgb_data
        self.fsim = self.fsim.to(a.device)
        ans = self.fsim(a, b, as_loss=False).item()
        
        return ans      

class NLPD_IQA(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards)
        from IQA_pytorch import NLPD
        self.chan = 1
        self.nlpd = NLPD(channels=self.chan)

    def calc(self, orig, rec):
        ans = 0.0
        if "Y" not in orig.yuv_data or "Y" not in rec.yuv_data:
            return -100.0
        if self.chan == 1:
            plane = "Y"
            a = orig.yuv_data[plane].unsqueeze(0).unsqueeze(0)
            b = rec.yuv_data[plane].unsqueeze(0).unsqueeze(0)
        elif self.chan == 3:
            a = DataClass.convert_yuvdict_to_tensor(orig.yuv_data, orig.yuv_data['Y'].device)
            b = DataClass.convert_yuvdict_to_tensor(rec.yuv_data, rec.yuv_data['Y'].device)
        self.nlpd = self.nlpd.to(a.device)
        ans = self.nlpd(a, b, as_loss=False).item()
        
        return ans   

class IWSSIM(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards)
        from IW_SSIM_PyTorch import IW_SSIM
        self.iwssim = IW_SSIM()

    def calc(self, orig, rec):
        ans = 0.0
        if "Y" not in orig.yuv_data or "Y" not in rec.yuv_data:
            return -100.0
        plane = "Y"
        # IW-SSIM takes input in a range 0-255
        a = orig.yuv_data[plane] * 255
        b = rec.yuv_data[plane] * 255
        ans = self.iwssim.test(a.detach().cpu().numpy(), b.detach().cpu().numpy())
        
        return ans.item()

class VMAF(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards)

    def calc(self, orig, rec):

        import tempfile
        import subprocess
        fp_o = tempfile.NamedTemporaryFile(delete=False)
        fp_r = tempfile.NamedTemporaryFile(delete=False)
        orig.write_yuv(fp_o, self.bits)
        rec.write_yuv(fp_r, self.bits)

        out_f =  tempfile.NamedTemporaryFile(delete=False)

        args = [
            "./vmaf.linux",  "-r", fp_o.name, "-d", fp_r.name, "-w", str(orig.shape[1]), "-h", str(orig.shape[0]), "-p", "444", "-b", str(self.bits), "-o", "vmaf.json", "--json"
        ]
        #os.system(f"./vmaf.linux -r {fp_o.name} -d {fp_r.name} -w {orig.shape[1]} -h {orig.shape[0]} -p 444 -b {self.bits} -o vmaf.json --json")
        subprocess.run(args,  stdout=subprocess.DEVNULL,  stderr=subprocess.DEVNULL)
        import json
        with open("vmaf.json", "r") as f:
            tmp = json.load(f)
        ans = tmp['frames'][0]['metrics']['vmaf']
        
        os.unlink(fp_o.name)
        os.unlink(fp_r.name)
        os.unlink(out_f.name)

        return ans      

class MetricsFabric:
    metrics_list = ["msssim_torch", "msssim_iqa", "psnr", "vif", "fsim", "nlpd", "iw-ssim", "vmaf", "psnr_hvs"]

    def __init__(self, bits={}, max_vals={}):
        self.bits = bits
        self.max_vals = max_vals

    def create_instance(self, name):
        name = name.lower()

        params = {}
        if isinstance(self.bits, dict):
            if name in self.bits:
                params["bits"] = self.bits[name]
            else:
                params["bits"] = 10
        else:
            params["bits"] = self.bits

        if isinstance(self.max_vals, dict):
            if name in self.max_vals:
                params["max_val"] = self.max_vals[name]
            else:
                params["max_val"] = (1 << params["bits"]) - 1
        else:
            params["max_val"] =  self.max_vals

        if name == "msssim_torch":
            return MSSSIMTorch(**params)
        elif name == "msssim_iqa":
            return MSSSIM_IQA(**params)
        elif name == "psnr":
            return PSNRMetric(**params)
        elif name == "vif":
            return VIF_IQA(**params)
        elif name == "fsim":
            return FSIM_IQA(**params)
        elif name == "nlpd":
            return NLPD_IQA(**params)
        elif name == "iw-ssim":
            return IWSSIM(**params)
        elif name == "vmaf":
            return VMAF(**params)
        elif name == "psnr_hvs":
            return PSNR_HVS(**params)
        else:
            raise NotImplementedError

