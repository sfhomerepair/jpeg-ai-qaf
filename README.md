# JPEG-AI metrics

JPEG-AI metrics calculation

## Structure of directories with results

Bitstreams and reconstructed files of different codecs should be organized as following:

    SUBMISSIONS
        CODEC1
            bit
                CODEC1_00016_TE_1744x2000_8bit_sRGB_200.bin
                …
            rec
                CODEC1_00016_TE_1744x2000_8bit_sRGB_200.png
                …
        ...
        CODEC2
            bit
                CODEC2_00016_TE_1744x2000_8bit_sRGB_200.bin
                …
            rec
                CODEC2_00016_TE_1744x2000_8bit_sRGB_200.png
                …                

Where CODEC1 and CODEC2 are the names of the codecs. ``bit`` and ``rec`` are the directories with bitstreams and reconstructed files.

## How to set up

1) clone the repository:  
2) go to your local copy of jpeg_ai_metrics: ``cd jpeg_ai_metrics``
3) create conda environment: ``conda create -n jpeg_ai_metrics python=3.6.7``
4) activate environment: ``conda activate jpeg_ai_metrics``
5) install dependencies: ``pip install -r requirements.txt``
## How to run 

Follow these steps in your local copy of aic:

1) activate environment: ``conda activate jpeg_ai_metrics``
2) ``python main.py <PATH_TO_ORIGs> <PATH_TO_BASE_DIR> <CODEC_NAME>``, where `<PATH_TO_ORIGs>` is a path to original files, `<PATH_TO_BASE_DIR>` is a path to base directory with results (`SUBMISSIONS` in a section `Structure of directories with results`), `<CODEC_NAME>` is a name of the codec under test.

By default all supported metrics are performed. If you would like to perform only some of them, set the list of necessary metrics by using command line parameter ``--metrics``. If you would like to calculate only PSNR and MSSSIM, add to command line the following arguments ``--metrics msssim_torch msssim_iqa psnr``.


See all available options by running: ``python main.py -h``

## Naming format for input files:


### Original images
`<Name>_<Width>x<Height>.png`

where `<Name>` is a name of the image file, `<Width>` and `<Height>` are width and height of the image

Example of the naming:
`00001_TE_1744x2000.png`

### Reconstrusted images

`<Codec>_<Name>_<Width>x<Height>_<bit>bit_<Format>_<QP>.png`

where `<Codec>` is a name of the codec, `<Name>` is a name of the image file, `<Width>` and `<Height>` are width and height of the image, `<bit>` is a bit-depth, `<Format>` is a format of the data (only YUV444 is currenlty supported), `<QP>` is a quality parameter.

Example of the naming:
`HEVC_00016_TE_1744x2000_8bit_sRGB_025.png`


### Bitrstream files

`<Codec>_<Name>_<Width>x<Height>_<bit>bit_<Format>_<QP>.bit`

where `<Codec>` is a name of the codec, `<Name>` is a name of the image file, `<Width>` and `<Height>` are width and height of the image, `<bit>` is a bit-depth (`8`), `<Format>` is a format of the data (`sRGB`), `<QP>` is a quality parameter.

Example of the naming:
`HEVC_00016_TE_1744x2000_8bit_sRGB_025.bit`

## Reporting template

The file `reporting_template.xls` is the official JPEG AI reporting template where BD rates are computed, RD plots are shown for several metrics and decoding complexity can be reported. 
