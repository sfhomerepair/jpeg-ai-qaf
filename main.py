import argparse
import os
from metrics import MetricsFabric, DataClass

def find_files(lst, prefix, ext):
    ans = []
    for fn in lst:
        if fn.startswith(prefix) and fn.endswith(ext):
            ans.append(fn)
    return ans


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument('orig', default=os.getcwd(), help="Path original YUV/PNG file(s)")
    ap.add_argument('base_dir', default=None, help="Path to the base directory with results of different codecs")
    ap.add_argument('codec', default=None, help="Name of the codec")
    ap.add_argument('-b', default=None, help="Path to bitstreams")
    ap.add_argument('-lst', default=None, help="Path list of YUV/PNG file(s) to be processed")
    ap.add_argument('-r', default=None, help="Path reconstructed YUV/PNG file(s)")
    ap.add_argument('-s', default=None, help="Path output file with statistics")
    ap.add_argument('--format', default="png", choices=["yuv", "png"], help="Format of input image files")
    ap.add_argument('--internal-bits', type=int, default=8, choices=[8,10], help='Bits for internal calculations')
    ap.add_argument("--jvet-psnr", default=False, action="store_true", help="Use 1020 as upper bound for 10 bits")
    ap.add_argument("--metrics", default=MetricsFabric.metrics_list, choices=MetricsFabric.metrics_list, nargs="+", help=f"Metrics to be used. Default: [{' '.join(MetricsFabric.metrics_list)}]")
    ap.add_argument("--metrics_output", default=MetricsFabric.metrics_list, choices=MetricsFabric.metrics_list, nargs="+", help=f"Order of metrics in output. Default: [{' '.join(MetricsFabric.metrics_list)}]")
    ap.add_argument('--bin-ext', default="bin", help="Extension of bin files")
    ap.add_argument('--color-conv', default="709", choices=["601", "709", "2020"], help="Color convertion notation")
    ap.add_argument('-v', default=False, action="store_true", help="Verbose mode")


    args = ap.parse_args()

    if args.r is None:
        args.r = os.path.join(args.base_dir, args.codec, "rec")
    if args.b is None:
        args.b = os.path.join(args.base_dir, args.codec, "bit")
    if args.s is None:
        args.s = f"{args.codec}_summary.txt"

    # TODO: add bits and max_vals to metric fabric
    bits = args.internal_bits
    if args.jvet_psnr:
        max_vals = {"psnr": 256 * (1 << (bits-8))}
    else:
        max_vals = (1 << bits) - 1

    metrics_fab = MetricsFabric(bits=bits, max_vals=max_vals)
    metrics = {}
    for m in args.metrics:
        metrics[m] = metrics_fab.create_instance(m)

    flst = []
    if args.lst is None or (not os.path.exists(args.lst)):
        #exit(f"Cannot find list file {args.lst}")
        flst = [x for x in os.listdir(args.orig) if os.path.splitext(x.lower())[1][1:] == args.format.lower()]
        flst = sorted(flst)
    else:
        with open(args.lst, "r") as iflst:
            for ifn in iflst:
                flst.append(ifn)

    lst_r = os.listdir(args.r)
    with  open(args.s, "w") as fs:
        for ifn in flst:
            ifn = ifn.strip()
            bn, ext = os.path.splitext(ifn)
            rec_fns = find_files(lst_r, f"{args.codec}_{bn}", ext)
            # TODO: round data here
            if args.v:
                print(f"Load original file {ifn}")
            data_o = DataClass().load_image(os.path.join(args.orig,ifn),def_bits=args.internal_bits, color_conv=args.color_conv)
            for rec_fn in sorted(rec_fns):
                bn_r, ext_r = os.path.splitext(rec_fn)
                bs_fn = os.path.join(args.b,f"{bn_r}.{args.bin_ext}")
                ans = [rec_fn]
                if args.v:
                    print(f"Start processing {rec_fn}. ", end="")
                if os.path.exists(bs_fn):
                    bs_size = os.path.getsize(bs_fn)
                    # TODO: round data here
                    data_r = DataClass().load_image(os.path.join(args.r,rec_fn),def_bits=args.internal_bits, color_conv=args.color_conv)
                    bpp = bs_size * 8 / (data_r.shape[0] * data_r.shape[1])
                    ans.append(str(bpp))
                    for m_t in args.metrics_output:
                        if m_t in metrics:
                            m = metrics[m_t]
                            tmp = m.calc(data_o, data_r)
                            if isinstance(tmp, list):
                                ans = ans + [str(x) for x in tmp]
                            else:
                                ans = ans + [str(tmp)]
                        else:
                            ans.append("-100")
                    if args.v:
                        print("Done.")
                else:
                    if args.v:
                        print(f"No bitstream file {bs_fn}!")

                fs.write("\t".join(ans) + "\n")
                fs.flush()



if __name__ == "__main__":
    main()